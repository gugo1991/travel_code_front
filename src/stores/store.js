import Vue from "vue";
import Vuex from "vuex";

import state from "./state";
import actions from "./actions";
import mutations from "./mutations";
import getters from "./getters";
import Auth from "./modules/auth/store";
import Layout from "./modules/layout/store";
import Users from "./modules/users/store";


Vue.use(Vuex);

export const store = new Vuex.Store({
  state,
  getters,
  mutations,
  actions,
  modules: {
    auth: Auth,
      layout: Layout,
      users: Users,
  }
});
