import * as mutation_types from "./mutation_types";

export default {
    [mutation_types.M_GET_GLOBAL_DATA](state, payload) {
        state.global_data = payload.data;
    }
};