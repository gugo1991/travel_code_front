import * as mutation_types from "./mutation_types";

export default {
  [mutation_types.M_ALL_USERS](state, payload) {
    state.users = payload.users;
  },
  [mutation_types.M_SEARCH_USERS](state, payload) {
    state.users = payload.users;
  },
  [mutation_types.M_GET_CITIES](state, payload) {
    state.cities = payload.cities;
  }
};
