import * as types from "./types";
import * as mutation_types from "./mutation_types";
import { AllUsersRequest, SearchUsers, GetCities } from "./api/methods";

export default {
  [types.ALL_USERS](context, payload) {
    AllUsersRequest()
      .then(
        response => {
          if (response.status == 200 && response.data) {
            context.commit(mutation_types.M_ALL_USERS, {
              users: response.data
            });
          } else {
            context.commit(mutation_types.M_ALL_USERS, { users: [] });
          }
        },
        error => {
          context.commit(mutation_types.M_ALL_USERS, { users: [] });
        }
      )
      .catch(error => {
        context.commit(mutation_types.M_ALL_USERS, { users: [] });
      });
  },
  [types.SEARCH_USERS](context, payload) {
    SearchUsers(payload)
      .then(
        response => {
          if (response.status == 200) {
            context.commit(mutation_types.M_SEARCH_USERS, {
              users: response.data
            });
          } else {
            context.commit(mutation_types.M_SEARCH_USERS, { users: [] });
          }
        },
        error => {
          context.commit(mutation_types.M_SEARCH_USERS, { users: [] });
        }
      )
      .catch(error => {
        context.commit(mutation_types.M_SEARCH_USERS, { users: [] });
      });
  },
  [types.GET_CITIES](context, payload) {
    GetCities()
      .then(
        response => {
          if (response.status == 200) {
            context.commit(mutation_types.M_GET_CITIES, {
              cities: response.data
            });
          } else {
            context.commit(mutation_types.M_GET_CITIES, { cities: [] });
          }
        },
        error => {
          context.commit(mutation_types.M_GET_CITIES, { cities: [] });
        }
      )
      .catch(error => {
        context.commit(mutation_types.M_GET_CITIES, { cities: [] });
      });
  }
};
