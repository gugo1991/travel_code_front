export default {
  getAllUsers: state => {
    return state.users || [];
  },
  getAllCities: state => {
    return state.cities || [];
  }
};
