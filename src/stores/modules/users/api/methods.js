import API_AllUsersRequest from "./all.users";
import API_SearchUsers from "./search.users";
import API_GetCities from "./get.cities";

export const AllUsersRequest = API_AllUsersRequest;
export const SearchUsers = API_SearchUsers;
export const GetCities = API_GetCities;