import Vue from "vue";


export default (data) => {
    return Vue.Api.Get(encodeURI(process.env.VUE_APP_API_URL + "/users?" + data));
};
