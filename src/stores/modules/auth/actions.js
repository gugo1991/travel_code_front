import * as types from "./types";
import * as mutation_types from "./mutation_types";
import { SignIn, SignUp, ConfirmPassword } from "./api/methods";

export default {
  [types.A_SIGN_IN](context, payload) {
    return new Promise((resolve, reject) => {
        SignIn(payload)
        .then(res => {
          console.log(res)
            if (res.status == 200) {
                context.commit(mutation_types.M_CONFIRM_PASSWORD, res.data);
                resolve(res);
          } else {
            reject(res);
          }
        })
        .catch(err => reject(err));
    });
  },
  [types.A_SIGN_UP](context, payload) {
    return new Promise((resolve, reject) => {
      SignUp(payload)
        .then(res => {
          if (res.status == 204) {
            resolve(1);
          } else {
            reject(0);
          }
        })
        .catch(err => reject(err));
    });
  },
  [types.A_CONFIRM_PASSWORD](context, payload) {
    console.log(payload);
    return new Promise((resolve, reject) => {
      ConfirmPassword(payload)
        .then(res => {
          console.log(res);
          if (res.status == 201) {
            context.commit(mutation_types.M_CONFIRM_PASSWORD, res.data);
          }
        })
        .catch(err => console.log(err));
    });
  }
};
