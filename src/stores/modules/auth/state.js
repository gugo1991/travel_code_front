export default {
    token: localStorage.getItem("token") || "",
    expired: localStorage.getItem("expired") || "",
    refreshToken: localStorage.getItem("refresh_token") || "",
};

