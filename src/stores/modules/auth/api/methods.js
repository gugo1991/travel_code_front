import API_Sign_Up from "./sign-up";
import API_Sign_In from "./sign-in";
import API_Confirm_Password from "./confirm-password";

export const SignUp = API_Sign_Up;
export const SignIn = API_Sign_In;
export const ConfirmPassword = API_Confirm_Password;
