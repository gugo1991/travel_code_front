export default {
  isAuthenticated: state => {
    if (state.token != "") {
      return true;
    } else {
      return false;
    }
  },
  authStatus: state => state.status
};
