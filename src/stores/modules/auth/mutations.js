import * as mutation_types from "./mutation_types";

export default {
  [mutation_types.M_CONFIRM_PASSWORD](state, payload) {
    state.token = payload.token_data.token;
    state.refreshToken = payload.token_data.refresh_token;
    state.expired = payload.token_data.lifetime;
    localStorage.setItem("user", JSON.stringify(payload));
    localStorage.setItem("token", payload.token_data.token);
    localStorage.setItem("refresh_token", payload.token_data.refresh_token);
    localStorage.setItem("expired", payload.token_data.lifetime);
  }
};
