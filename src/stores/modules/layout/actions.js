import * as types from "./types";
import * as mutation_types from "./mutation_types";

export default {
  [types.CLOSE_SIDEBAR](context, payload) {
    context.commit(mutation_types.M_CLOSE_SIDEBAR);
  },
  [types.CLOSE_SIDEBAR_USERS](context, payload) {
    context.commit(mutation_types.M_CLOSE_SIDEBAR_USERS);
  },
  [types.CLOSE_SIDEBAR_RIGHT](context, payload) {
    context.commit(mutation_types.M_CLOSE_SIDEBAR_RIGHT);
  },
  [types.TOGGLE_RIGHT_SIDEBAR_MENU](context, payload) {
    context.commit(mutation_types.M_TOGGLE_SIDEBAR_MENU, payload);
  },
  [types.TOGGLE_RIGHT_SIDEBAR_MENU_AGENTSTVO](context, payload) {
    context.commit(mutation_types.M_TOGGLE_SIDEBAR_MENU_AGENTSTVO, payload);
  },
  [types.TOGGLE_RIGHT_SIDEBAR_MENU_ADMIN](context, payload) {
    context.commit(mutation_types.M_TOGGLE_SIDEBAR_MENU_ADMIN, payload);
  },
  [types.SET_USER_GROUP](context, payload) {
    context.commit(mutation_types.M_SET_USER_GROUP, payload);
  },

    [types.UPLOAD_BG_AVATAR](context, payload) {

    }
};
