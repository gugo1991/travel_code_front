export default {
  sideBarWidth: false,
  sideBarWidthRight: true,
  agentProfilePage: "MAIN",
  agentstvoProfilePage: "MAIN",
  sideBarWidthUsers: true,
  sideBarUsersGroup: {
    group: "",
    userId: ""
  },
  adminProfilePage: "MAIN",
  documents: [],
  agencyManager: {
    type: false,
    managerId: null
  }
};
