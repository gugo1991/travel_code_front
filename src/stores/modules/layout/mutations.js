import * as mutation_types from "../layout/mutation_types";
export default {
  [mutation_types.M_CLOSE_SIDEBAR](state, payload) {
    if (state.sideBarWidth) {
      state.sideBarWidth = false;
    } else {
      state.sideBarWidth = true;
    }
  },
  [mutation_types.M_CLOSE_SIDEBAR_USERS](state, payload) {
    if (state.sideBarWidthUsers) {
      state.sideBarWidthUsers = false;
    } else {
      state.sideBarWidthUsers = true;
    }
  },
  [mutation_types.M_CLOSE_SIDEBAR_RIGHT](state, payload) {
    if (state.sideBarWidthRight) {
      state.sideBarWidthRight = false;
    } else {
      state.sideBarWidthRight = true;
    }
  },
  [mutation_types.M_TOGGLE_SIDEBAR_MENU](state, payload) {
    state.agentProfilePage = payload.menu;
  },
  [mutation_types.M_SET_USER_GROUP](state, payload) {
    state.sideBarUsersGroup.group = payload.group;
    state.sideBarUsersGroup.userId = payload.userId;
  },
  [mutation_types.M_TOGGLE_SIDEBAR_MENU_AGENTSTVO](state, payload) {
    state.agentstvoProfilePage = payload.menu;
  },
  [mutation_types.M_TOGGLE_SIDEBAR_MENU_ADMIN](state, payload) {
    state.adminProfilePage = payload.menu;
  },

};
