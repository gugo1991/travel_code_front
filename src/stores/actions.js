import * as types from "./types";
import * as mutation_types from "./mutation_types";
import Vue from "vue";

export default {
    [types.A_GET_GLOBAL_DATA](context, payload) {
        let options = {};
        let o = payload;
        options["headers"] = { "Cache-Control": "no-cache" };
        options["headers"] = { "Content-Type": "application/json" };
        Vue.Api.Get(
            "http://5.189.227.112/all_data",
            options
        )
            .then(res => {
                console.log(res)
                context.commit(mutation_types.M_GET_GLOBAL_DATA, res)
        })
    },
}