import Vue from "vue";
import Vuetify from "vuetify/lib";
import {VApp, VBtn, VBtnToggle,VExpansionPanel, VExpansionPanelContent, transitions } from "vuetify";

import "vuetify/src/stylus/app.styl";

Vue.use(Vuetify, {
    iconfont: "md",
    components: {
        VApp,
        VBtnToggle,
        VBtn,
        transitions
    }
});
