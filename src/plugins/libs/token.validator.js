import * as UserServiceActionTypes from "@/stores/modules/user/types";
import UserServiceAction from "@/stores/modules/user/actions";
import Promise from "promise";

const TokenValidatorPlugin = {
  install(Vue, options) {
    Vue.token = Vue.prototype.$token = {
      get() {
        if (!Vue.hasOwnProperty("cookie")) {
          return;
        }
        return localStorage.getItem("user-token");
      },
      check(token) {
        console.log("CHECK");
        return new Promise((resolve, reject) => {

          let validate_result = {
            isValid: false,
            isExpired: false,
            response: {}
          };

          if (token != undefined && token != null && token != "") {
            console.log("CHECK 4");

            Vue.Api.Post("http://api.smrkt.plus/auth/refresh-token", null, {
              headers: { "X-Authorization": "Bearer " + token }
            }).then(
              result => {
                console.log(result);
                if (result.status == 401) {
                  validate_result.isExpired = true;
                  //Vue.store.dispatch('user/'+UserServiceActionTypes.A_LOGOUT);
                } else {
                  validate_result.isValid = true;
                  validate_result.response = {
                    token: token,
                  };
                }

                console.log(result);

                resolve(validate_result);
              },
              error => {
                console.log("/validate error");
                validate_result.isExpired = true;

                resolve(validate_result);
              }
            );
          } else {
            console.log("CHECK 3 EXIT");
            resolve(validate_result);
          }
        });
      }
    };
  }
};

export default TokenValidatorPlugin;
