import Vue from "vue";
import Router from "vue-router";
import SignIn from "./components/auth/SignIn";
import SignUp from "./components/auth/SignUp";
import ConfirmPassword from "./components/auth/ConfirmPassword";
import Dashboard from "./components/admin/Dashboard";
import * as mutation_types from "./stores/modules/auth/mutation_types";
import * as types from "@/stores/modules/auth/types";
import { store } from "./stores/store";

Vue.use(Router);

let router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/sign-in",
      name: "sign-in",
      component: SignIn,
      meta: { closeForAuth: true, layout: "no-sidebar" }
    },
    {
      path: "/sign-up",
      name: "sign-up",
      component: SignUp,
      meta: { closeForAuth: true, layout: "no-sidebar" }
    },
    {
      path: "/confirm-password",
      name: "confirm-password",
      component: ConfirmPassword
    },
    {
      path: "/dashboard",
      name: "dashboard",
      component: Dashboard,
      meta: { requiresAuth: true }
    }
  ]
});
router.beforeEach((to, from, next) => {
  const token = localStorage.getItem("token");
  let ValidateRouter = () => {
    if (token != undefined || token != "") {
      if (to.matched.some(record => record.meta.requiresAuth)) {
        if (store.getters["auth/isAuthenticated"]) {
          console.log("ete authenticate");
          next();
        } else {
          console.log("ete authenticate CHI");
          next("sign-in");
        }
      } else if (to.matched.some(record => record.meta.closeForAuth)) {
        if (
          store.getters["auth/isAuthenticated"] != false &&
          store.getters["auth/isAuthenticated"] != undefined
        ) {
          console.log("ete authenticate ELSE IF");
          next({
            path: "/"
          });
        } else {
          console.log("ete authenticate else ELSE IF");
          next();
        }
      } else {
        console.log("ete authenticate ELSE IF");
        next();
      }
    } else {
      next("sign-in");
    }
  };
  if (token != undefined && token != "") {
    const expired = localStorage.getItem("expired");
    const refreshToken = localStorage.getItem("refresh_token");
    var seconds = new Date().getTime() / 1000;
    if (expired <= seconds) {
      console.log("Ancela Jamkety");
      Vue.Api.Post(
        "http://5.189.227.112/user/update_access_token",
        { refresh_token: refreshToken },
        {
          headers: { "X-Authorization": "Bearer " + token }
        }
      ).then(
        result => {
          console.log(result);
          if (result.status == 200) {
            localStorage.setItem("token", result.data.accessToken);
            localStorage.setItem("expired", result.data.lifetime);
            localStorage.setItem("refresh_token", result.data.refreshToken);
            Vue.$store.dispatch("auth/" + types.A_LOGIN_STORAGE);
          } else {
            console.log("ELSA MTEL");
          }

          console.log(result);

          resolve();
        },
        error => {
          console.log("/validate error");

          resolve();
        }
      );
    } else {
      console.log("Gortuma");
    }

    ValidateRouter();
  } else {
    ValidateRouter();
  }
});

export default router;
