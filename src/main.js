import Vue from "vue";
import "./plugins/vuetify";
import App from "./App.vue";
import router from "./router";
import { store } from "./stores/store";

Vue.config.productionTip = false;
require("./plugins/setup.plugins");

import Default from "./layouts/Default.vue";
import NoSideBar from "./layouts/NoSideBar.vue";

Vue.component("default-layout", Default);
Vue.component("no-sidebar-layout", NoSideBar);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
